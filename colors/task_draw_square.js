var wrap = document.querySelector('.wrap'),
    colors = ['red', 'blue', 'green', 'yellow', 'black', '#F08080', '#FFA07A', '#FF4500', '#FF00FF', '#4B0082', '#BC8F8F', '#800000', '#778899', '#00FFFF', '#B8860B', '#DA70D6'],
    start = document.getElementById('start');

function Square(id) {
  var newSq = document.createElement('div');
  newSq.classList.add('square');
  newSq.setAttribute('data-id', id);
  wrap.appendChild(newSq);
}
//create blocks
for (var i=0; i< 16; i++) {
  new Square(i);
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

var current = 3;
var blocks = document.querySelectorAll('.square'),
    level = document.querySelector('.level-number'),
    sound = new Audio();
var selected = [];
var lives = 3;
var error = 0;
sound.src = 'sounds/error.mp3';
sound.load();


function drawLives(number) {
  var heart = document.querySelector('.tries');
  heart.innerHTML = '';
  for (var i=0; i<number; i++) {
    var live = document.createElement('img');
    live.src = 'images/heart.png';
    heart.appendChild(live);
  }
}


function checkLives() {
  if (lives == 0) {
    alert('You lose');
    location.reload();
  }

}

function startCycle() {
      console.log('startCycle');
      level.innerHTML = current-2;
      for (var i=0; i<current; i++) {
        var random = Math.floor(Math.random()*16);
        //check if random is repeated
        while (selected.indexOf(random) != -1) {
          random = Math.floor(Math.random()*16);
        }
        selected.push(random);


      }
      console.log(selected);
      var key = 0;
      var timerId = setInterval(function drawBlock() {
        console.log(selected[key]);
        blocks[selected[key]].style.backgroundColor = colors[key];
        if ((key+1)==current) {clearInterval(timerId); }
        else key++;
      }, 500);

}

var x = 0;

function checkTarget(arr) {
    wrap.onclick = function(event) {
    if (event.target.getAttribute('data-id') == arr[arr.length-1]) {
      event.target.style.backgroundColor = '';
      arr.pop();
    }
    else {
      error+=1;
      lives -=1;
      drawLives(lives);
      checkLives();
      sound.play();
      }
  }
  start.disabled = false;
}

function nextStart() {
  console.log('click');
  if (error == 0) {
    lives++;
  }
  current++;
  var selected = [];
  error = 0;
  StartGame();
}

start.onclick = nextStart;

function StartGame() {
    start.disabled = true;
    shuffleArray(colors);
    drawLives(lives);
    startCycle(current);
    checkTarget(selected);


}

StartGame();
